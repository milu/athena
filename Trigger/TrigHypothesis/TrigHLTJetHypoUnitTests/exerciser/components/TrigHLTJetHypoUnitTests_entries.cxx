/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#include "TrigHLTJetHypoUnitTests/JetHypoExerciserAlg.h"
#include "TrigHLTJetHypoUnitTests/JetHypoExerciserCompareAlg.h"
#include "TrigHLTJetHypoUnitTests/SimpleHypoJetVectorGenerator.h"
#include "TrigHLTJetHypoUnitTests/AgreeHelperTool.h"

DECLARE_COMPONENT(JetHypoExerciserAlg)
DECLARE_COMPONENT(JetHypoExerciserCompareAlg)
DECLARE_COMPONENT(SimpleHypoJetVectorGenerator)
DECLARE_COMPONENT(AgreeHelperTool)
